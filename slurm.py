#
# Slurm.py
#
#  A module to wrap Slurm commands
#
#
#

from datetime import datetime
import json
import re
import sys
import subprocess

class Slurm( object ):

    fmt = '%Y-%m-%dT%H:%M:%S'

    def csv_list_load( self, lines, translate ):
        ''' load from an open file '''

        headers = []
        rows = []

        for line in lines:

            # if the line starts with a '#' then ignore
            if line.startswith('#'):
                continue

            # split on the CSV delimiter
            data = line.split('|')

            if len( headers ) == 0:
                # first row is the header
                headers = map( lambda x: x.lower(), data )
                continue

            # ensure the count of data and header fields match
            if len( data ) != len( headers ):
                raise Exception("len difference")

            hash = {}

            for i in range( len( data ) ):

                key = headers[ i ]

                # translate the file if a translation operation is defined
                hash[ key ] = translate[ key ]( data[ i ] ) if key in translate else data[ i ]

                #if key in translate:
                    #hash[ key ] = translate[ key ]( data[ i ] )
                #else:
                    #hash[ key ] = data[ i ]

            self.record( hash )


    def csv_file_load( self, fname, translate = {} ):
        ''' load from a CSV file '''

        with open( fname ) as fp:
            return self.csv_list_load( [ line.rstrip('\n') for line in fp.readlines() ], translate )

    def process_load( self, cmd, translate = {} ):
        ''' load from a subprocess '''

        if not isinstance( cmd, (list,tuple) ):
            cmd = cmd.split(' ')

        proc = subprocess.Popen( cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        ( out, err ) = proc.communicate()

        if proc.returncode != 0:
            raise Exception("command failed")

        return self.csv_list_load( out.splitlines(), translate )

    def record( self, data ):
        '''
            A user defined class will override this method to receive data
        '''
        raise Exception("override the 'record' method to receive data")

    #### COMMON TRANSLATION METHODS ####

    @staticmethod
    def toInt( value ):
        ''' value is returned as an int if it is an int, otherwise None '''
        try:
            return( int( value ) )
        except ValueError:
            return( None )

    @staticmethod
    def toArray( value ):
        ''' convert the value to a list split my commas '''
        if value == '':
            return []
        else:
            return( value.split(',') )

    @staticmethod
    def toGres( value ):
        ''' TODO '''
        if value == '':
            return None
        else:
            return value

    @staticmethod
    def toTres( value ):
        '''
            Convert a TRES definition to a hash
        '''

        hash = {}

        for tmp in filter( lambda x: x != '', value.split(',') ):
            (k,v) = tmp.split('=',2)
            hash[ k ] = v

        return hash

    @staticmethod
    def toState( value ):
        '''
            strip extraneous data to return the pure state
            (ie: cancelled jobs may have extra information ie 'CANCELLED by 800078)
        '''

        # so remove everything from an optional first space, then translate to lowercase
        return( value.split(' ',1)[0].lower() )

    @staticmethod
    def toNodeList( value ):
        '''
            translate a compacted nodelist to a list of node names

            ie: 'abc123-[1,2]' becomes [ 'abc123-1', 'abc123-2' ]
                ie: host-[23,24] => [ host-23, host-24 ]
                    host-[1-3]-[2,4] => [ host-1-2, host-1-4, host-2-2, host-2-4 ... ]
        '''

        def expandIndexes( string ):
            """ checks for numeric range expansion (ie: converts 10-23 into a list 10, 11, .. , 23 """

            m = re.match('(\d+)-(\d+)', string )
            if m is None:
                return [ string ]
            elif len( m.groups() ) != 2:
                raise ValueError("Expected to match two numbers")
            else:
                lower = m.group(1)
                upper = m.group(2)

                l = len( lower )
                if len( upper ) != l:
                    raise ValueError("for ranges both numbers must have the same character width")

                fmt = "%%0%dd" % l

                values = []

                for value in range( int( lower ), int( upper ) + 1 ):
                    values.append( fmt % value )

                return values

        def recurseExpandElement( elements ):
            """ single host name expanded processing """

            node = ''

            while len( elements ) > 0:

                part = elements.pop(0)

                if part != '[':
                    node += part
                    continue

                # Found the start of a bracket set . . 

                indexes = elements.pop(0)

                if elements.pop(0) != ']':
                    raise ValueError("Closing ']' missing from expansion array")

                numbers = []

                for index in indexes.split(','):
                    numbers += expandIndexes( index )

                values = []

                for number in numbers:
                    newpart = node + number
                    values += recurseExpandElement( [ newpart ] + elements )
    
                return values

            return( [ node ] )

        ''' start of toNodeList code '''

        nodelist = []
 
        for node in re.compile("(?![^)(]*\([^)(]*?\)\)),(?![^\[]*\])").split( value ):

            elements = filter(lambda a: a != '', re.compile("(.+?)(\[)(.*?)(\])").split( node ) )

            if len( elements ) == 1:
                nodelist += elements
            elif len( elements ) == 0:
                raise ValueError("Unable to process string '%s'" % node )
            else:
                nodelist += recurseExpandElement( elements )

        return nodelist

    @staticmethod
    def toEpoch( value ):
        ''' convert a Slurm timestamp string to Unix epoch '''
        # Test Data: 2020-04-26T22:12:47 -> 1587939167

        # to make this more general, if an integer is presented
        # then assume it is already in Unix epoch format

        if value == 'Unknown':
            return None

        try:
            return( int( value ) )
        except ValueError:
            pass

        try:
            # convert from the Slurm timestamp format
            date = datetime.strptime( value , Slurm.fmt ).replace( tzinfo = None )
            epoch = datetime( 1970, 1, 1 )
            return int( ( date - epoch ).total_seconds() )
        except ValueError:
            raise ValueError( "timestamp '%s' isn't in the Slurm format: %s" % ( value, Slurm.fmt ))

    @staticmethod
    def toSlurmTimestamp( value ):
        ''' convert a unix epoch to a Slurm timestamp string '''

        try:
            return( datetime.utcfromtimestamp( value ).strftime( Slurm.fmt ) )
        except TypeError:
            pass

        try:
            # Is it already in the Slurm format?
            datetime.strptime( value , Slurm.fmt )
            return value
        except ValueError as e:
            raise ValueError("Date %s is not in the Slurm format: %s" % ( value, str( e ) ))

class sAcct( Slurm ):
    '''
        wrap Slurm "sacct" usage.
    '''

    fields = [
            'tresusageinmin', 'reqtres', 'partition', 'avediskwrite',
            'tresusageinmax', 'tresusageoutave', 'maxdiskwritetask',
            'ntasks', 'mincputask', 'avecpu', 'reqcpufreqmax', 'maxdiskwritenode',
            'avecpufreq', 'reqmem', 'maxpagesnode', 'maxpagestask', 'maxdiskreadnode',
            'mincpunode', 'maxdiskreadtask', 'jobidraw', 'state', 'reqcpufreqmin',
            'elapsed', 'maxvmsize', 'maxpages', 'tresusageinmintask', 'maxrssnode',
            'allocgres', 'tresusageouttot', 'reqcpufreqgov', 'reqgres', 'maxdiskread',
            'maxrsstask', 'maxrss', 'maxvmsizetask', 'tresusageinmaxnode', 'alloctres',
            'tresusageintot', 'jobname', 'avediskread', 'averss', 'tresusageinminnode',
            'tresusageoutmax', 'maxvmsizenode', 'mincpu', 'maxdiskwrite', 'tresusageoutmaxtask',
            'avevmsize', 'avepages', 'jobid', 'alloccpus', 'tresusageinmaxtask',
            'consumedenergy', 'tresusageinave', 'tresusageoutmaxnode', 'exitcode',
            'start','end','submit','nodelist'
            ]

    fmap = {
        'partition': Slurm.toArray,
        'state': Slurm.toState,
        'nodelist': Slurm.toNodeList,
        'submit': Slurm.toEpoch,
        'start': Slurm.toEpoch,
        'end': Slurm.toEpoch,
        'alloctres': Slurm.toTres,
        'allocgres': Slurm.toGres,
        'tresusageinave': Slurm.toTres,
        'tresusageoutmax': Slurm.toTres,
        'tresusageinminnode': Slurm.toTres,
        'tresusageinmaxtask': Slurm.toTres,
        'tresusageintot': Slurm.toTres,
        'tresusageouttot': Slurm.toTres,
        'tresusageinmaxnode': Slurm.toTres,
        'tresusageinmintask': Slurm.toTres,
        'tresusageinmin': Slurm.toTres,
        'reqtres': Slurm.toTres,
        'tresusageinmax': Slurm.toTres,
        'tresusageoutave': Slurm.toTres,
        'tresusageoutmaxnode': Slurm.toTres,
        'avevmsize': Slurm.toInt
    }

    def load( self, fname ):
        ''' load sacct data from a CSV file '''
        return self.csv_file_load( fname, sAcct.fmap )

    def query( self, start = None, end = None, fields = fmap.keys ):
        ''' load sacct data from running "sacct" '''

        cmd = [ 'sacct', '-P', '--noconvert' ]
        if start is not None:
            cmd += [ '-S', Slurm.toSlurmTimestamp( start ) ]
        if end is not None:
            cmd += [ '-E', Slurm.toSlurmTimestamp( end ) ]
        cmd += [ '--format', ','.join( sAcct.fields ) ]

        return self.process_load( cmd, sAcct.fmap )


sacct = sAcct()

# END OF FILE
