#!/usr/bin/python

'''
    Script to analyse GPU utilisation for the defined nodes

    !!WARNING!!
        The logic isn't correct for jobs that span more than
        one node. If a job does span more than one node then
        an exception will be thrown.

'''


import json
import sys

from datetime import datetime
import slurm

# these nodes have the rtx2080Ti GPUs
nodelist = slurm.Slurm.toNodeList( 'gpu510-[02,07,12,17]' )

# function to see if any elements of l1 are in l2
def contains( l1, l2 ):
    for l in l1:
        if l in l2:
            return True
    return False

# output store
store = {}

# when slurm.sAcct is processing rows the method
# 'record' is invoked for each row. 

class mySacct( slurm.sAcct ):

    def record( self, data ):

        # don't process .batch and .extern
        if '.' in data['jobid']:
            return

        if 'nodelist' not in data:
            return

        if data['start'] is None:
            return

        if not contains( nodelist, data['nodelist'] ):
            return

        if 'gres/gpu' not in data['alloctres']:
            print json.dumps( data, indent = 4 )
            raise Exception( "can't find GPU allocation" )

        if len( data['nodelist'] ) > 1:
            print "WARNING! GPU job %s spans more than one node - ignored" % data['jobidraw']
            next

        gpu = int( data['alloctres']['gres/gpu'] )

        for n in data['nodelist']:
            if n not in store:
                store[ n ] = {}

        # ensure that the 'start' and 'end' times are present in the store
        for t in [ data['start'], data['end'] ]:
            if t not in store[ n ]:
                store[ n ][ t ] = 0

        # format is store[ <nodename> ][ <time since epoch> ] = <gpu delta>
        # for a 'start' time the GPU delta is increased
        # for an 'end' time the GPU delta is decreased

        store[ n ][ data['start'] ] += gpu
        store[ n ][ data['end'] ] -= gpu

sacct = mySacct()

s = slurm.Slurm.toEpoch( '2020-04-19T00:00:00' )
e = slurm.Slurm.toEpoch( '2020-04-26T00:00:00' )

# load from a CSV file (output from sacct using -P)
# sacct.load( 'sacct-output.csv' )

# or directly load by running sacct
sacct.query( start = s, end = e )

# data is now in "store", so iterate over it

for node,data in sorted( store.iteritems() ):
    '''
        data = {
                t1: <gpu active delta>
                t2: <gpu active delta>
                ...
                tn: <gpu active delta>
                }

        iterate over the ordered keys (time) tallying the number
        of GPU seconds (gpus active * time delta) that were active
    '''

    active = 0
    cummulative = 0

    # define the start and end times as data points
    # with a 0 delta if they are not already present.

    for when in [ s, e ]:
        if when not in data:
            data[ when ] = 0

    lt = s

    within = False

    for now in sorted( data.keys() ):

        # keep track of times?
        if s < now and now <= e:
            # += time delta since last sample * number of active GPUs
            cummulative += (now - lt) * active

        # update the active GPU count
        active += data[ now ]

        # update the 'last' sample time
        lt = now

    percent = cummulative * 100 / (( e - s ) * 8 )

    print "%s %d GPU seconds (%d%% of period)" % ( node, cummulative, percent )

